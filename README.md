# human

Package human provides functions to format units to human friendly strings. The idea of this package is loosely based on `https://github.com/dustin/go-humanize`, but with a more European approach.
