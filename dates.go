package human

import "time"

// Date returns a human friendly date representation for input as 'yyyy-MM-dd'.
func Date(input time.Time) string {
	return input.Format("2006-01-02")
}

// DateTime returns a human friendly date+time representation for input as 'yyyy-MM-dd HH:mm:ss'.
func DateTime(input time.Time) string {
	return input.Format("2006-01-02 15:04:05")
}
