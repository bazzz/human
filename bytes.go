package human

import (
	"fmt"
	"math"
)

// Bytes returns a human friendly size representation for input as SI units in bytes.
func Bytes(input uint64) string {
	if input < 10 {
		return fmt.Sprintf("%d B", input)
	}
	sizes := []string{"B", "kB", "MB", "GB", "TB", "PB", "EB"}
	base := 1000.0
	e := math.Floor(math.Log(float64(input)) / math.Log(base))
	suffix := sizes[int(e)]
	val := math.Floor(float64(input)/math.Pow(base, e)*10+0.5) / 10
	f := "%.0f %s"
	if val < 10 {
		f = "%.1f %s"
	}
	return fmt.Sprintf(f, val, suffix)
}
