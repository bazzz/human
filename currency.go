package human

import (
	"strconv"
	"strings"
)

// Currency returns a human friendly currency representation for input as money in Euros.
func Currency(input float64) string {
	return "€ " + strings.Replace(strconv.FormatFloat(input, 'f', 2, 64), ".", ",", 1)
}
